package com.dxl.bean;

/*
 * 与消息表对应的实体类
 * 
*/
public class Message {
	/*
	 * 主键
	 * */
	private String id;
	/*
	 * 指令名
	 * */
	private String COMMAND;
	/*
	 * 描述
	 * */
	private String DESCRIPTION;
	/*
	 * 内容
	 * */
	private String CONTENT;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCOMMAND() {
		return COMMAND;
	}
	public void setCOMMAND(String cOMMAND) {
		COMMAND = cOMMAND;
	}
	public String getDESCRIPTION() {
		return DESCRIPTION;
	}
	public void setDESCRIPTION(String dESCRIPTION) {
		DESCRIPTION = dESCRIPTION;
	}
	public String getCONTENT() {
		return CONTENT;
	}
	public void setCONTENT(String cONTENT) {
		CONTENT = cONTENT;
	}
	
	
}
