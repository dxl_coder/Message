package com.dxl.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dxl.bean.Message;
import com.dxl.service.ListService;

@SuppressWarnings("serial")
public class ListServlet extends HttpServlet{
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		//设置编码格式
		req.setCharacterEncoding("UTF-8");
		
		//接受前端传过来的参数值
		String command = req.getParameter("COMMAND");
		
		String desription = req.getParameter("DESCRIPTION");
		
		//向前端传值
		req.setAttribute("command", command);
		
		req.setAttribute("desription", desription);
		
		//查询消息列表并传给页面
		ListService listService = new ListService();
		
		
		req.setAttribute("messagelist", listService.queryMessagelist(command, desription));
		
		//向页面跳转
		req.getRequestDispatcher("/WEB-INF/jsp/back/list.jsp").forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.doGet(req, resp);
	}
}
