package com.dxl.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.dxl.bean.Message;
import com.dxl.db.DBAccess;

/*
 * 和message表相关的数据表操作
 * 
 * */

public class MessageDao {
	/*
	 * jdbc
	*根据查询条件查询消息列表
	*/
/*	public List<Message> queryMessagelist(String command , String desription)  {
		//设置储存数据库地址，用户名和密码的变量
				String url , username , passwd ;

				url = "jdbc:mysql://127.0.0.1:3306/micro_message?characterEncoding=utf8";
				username = "root";
				passwd = "";
				
				List<Message> messagelist = new ArrayList<Message>();
				
				try {
					Class.forName("com.mysql.jdbc.Driver");
					Connection  conn =  
							DriverManager.getConnection(url,username,passwd);
					
					StringBuilder sql = 
							new StringBuilder("select id , COMMAND , DESCRIPTION , CONTENT from message where 1=1 "); 
					
					//建立有一个list，先缓存值，才能查找
					List<String> paramlist = new ArrayList<String>();
					
					if(command != null && !"".equals(command.trim())) {
						sql.append(" and COMMAND = ? ");
						paramlist.add(command);
					}
					
					if(desription != null && !"".equals(desription.trim())) {
						sql.append(" and DESCRIPTION like '%' ? '%' ");
						paramlist.add(desription);	
					}
					
					
					PreparedStatement ps = conn.prepareStatement(sql.toString());
					
					//遍历出paramlilstd里的值
					for( int i = 0 ; i < paramlist.size() ; i++) {
						ps.setString( i +1 , paramlist.get(i));
					}
					
					ResultSet rs = ps.executeQuery();
					
					
					
					while(rs.next()) {
						Message message = new Message();
						messagelist.add(message);
						message.setId(rs.getString("id"));
						message.setCOMMAND(rs.getString("COMMAND"));
						message.setDESCRIPTION(rs.getString("DESCRIPTION"));
						message.setCONTENT(rs.getString("CONTENT"));
					}
					
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return messagelist;
	}*/
	
	//Mybatis
	/*
	 * Mybatis
	*根据查询条件查询消息列表
	*/
	public List<Message> 
		queryMessagelist(String command , String desription){
		
		//获取访问数据库的信息
		DBAccess dbAccess = new DBAccess();
		//创建list保存sqlSession查询到的值
		List<Message> messagelist = new ArrayList<Message>();
		
		SqlSession sqlSession = null;
		try {
			sqlSession = dbAccess.getSqlSession();
			
			//通过sqlSession执行sql语句
			//queryMessagelist是sql配置文件中select的id属性中的名称
			//通过配置文件里的命名空间名 点语句id调用sql语句
			messagelist = sqlSession.selectList("Message.queryMessagelist");
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if(sqlSession != null) {
				sqlSession.close();
			}
		}
		
		return messagelist;
		
	}
	
	/*public static void main(String[] args) {
		MessageDao messageDao 
				= new MessageDao();
		messageDao.queryMessagelist("", "");
	}*/
	
}
