package com.dxl.service;

import java.util.List;

import com.dxl.bean.Message;
import com.dxl.dao.MessageDao;

/*
*列表相关的业务功能
*/
public class ListService {
	public List<Message> queryMessagelist(String command , String desription) {
	
		MessageDao messageDao = new MessageDao();
		
		return messageDao.queryMessagelist(command, desription);
		
	}
	

}
